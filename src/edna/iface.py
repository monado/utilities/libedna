# Copyright 2022, Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0

from dataclasses import dataclass
import clang.cindex


@dataclass
class Arg:
    """
    A arugment to a function or method.
    """

    name: str
    c_decl_str: str
    clang_type: clang.cindex.Type
    clang_decl: clang.cindex.Cursor


@dataclass
class Ret:
    """
    A return value from a function or method.
    """

    is_void: bool
    c_decl_str: str
    clang_type: clang.cindex.Type


@dataclass
class Method:
    """
    A single C style struct method call.
    """

    name: str
    ret: Ret
    args: list[Arg]


@dataclass
class StructInterface:
    """
    A C struct exposing a C++ class like interface.
    """

    name: str
    methods: list[Method]
    clang_decl: clang.cindex.Cursor
