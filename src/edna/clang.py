# Copyright 2022, Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0

from edna.iface import StructInterface, Method, Arg, Ret

import clang.cindex
import typing

def filter_node_list_by_node_kind(
        nodes: typing.Iterable[clang.cindex.Cursor],
        kinds: list[clang.cindex.CursorKind]) -> typing.Iterable[clang.cindex.Cursor]:
    result = []
    for i in nodes:
        if i.kind in kinds:
            result.append(i)
    return result


def is_field_decl(node: clang.cindex.Cursor) -> bool:
    if node.kind != clang.cindex.CursorKind.FIELD_DECL:
        return False
    return True


def is_function_pointer_type(type: clang.cindex.Type) -> bool:
    if type.kind != clang.cindex.TypeKind.POINTER:
        return False
    if type.get_pointee().kind != clang.cindex.TypeKind.FUNCTIONPROTO:
        return False
    return True


def is_function_pointer_void_return(type: clang.cindex.Type) -> bool:
    """
    Takes a pointer to a function proto type and returns true if it returs void.
    """
    if not is_function_pointer_type(type):
        print("Expected function pointer, but isn't a pointer!")
        return False

    result = type.get_pointee().get_result()
    if result.get_canonical().kind != clang.cindex.TypeKind.VOID:
        return False

    return True


def get_function_pointer_decls_list(node: clang.cindex.Cursor) -> list[clang.cindex.Cursor]:
    l: list = []
    for c in node.get_children():
        if not is_field_decl(c):
            continue
        if not is_function_pointer_type(c.type):
            continue
        l.append(c)

    return l


def stringify_argument_type_and_name(type: clang.cindex.Type, name: str) -> str:
    start: str = ''
    end: str = ''
    cur: type = type

    while True:
        if (cur.kind == clang.cindex.TypeKind.CONSTANTARRAY):
            assert not cur.is_const_qualified(), "Array type is const"
            end = f'{end}[{cur.get_array_size()}]'
            cur = cur.get_array_element_type()
            continue
        if (cur.kind == clang.cindex.TypeKind.INCOMPLETEARRAY):
            assert not cur.is_const_qualified(), "Array type is const"
            end = end + '[]'
            cur = cur.get_array_element_type()
            continue
        if (cur.kind == clang.cindex.TypeKind.POINTER):
            if cur.is_const_qualified():
                start = '*const ' + start
            else:
                start = '*' + start
            cur = cur.get_pointee()
            continue
        break
    return cur.spelling + ' ' + start + name + end


def get_struct_interface_from_struct(struct: clang.cindex.Cursor) -> StructInterface:

    l = get_function_pointer_decls_list(struct)

    methods: list[Method] = []
    for c in l:
        ret_is_void: bool = is_function_pointer_void_return(c.type)
        ret_clang_type = c.type.get_pointee().get_result()

        # Get the argument list
        children = list(c.get_children())
        # If the return value is a typedef lookup then it will be first in the list.
        if len(children) > 0 and children[0].kind != clang.cindex.CursorKind.PARM_DECL:
            children = children[1:]

        ret: Ret = Ret(
            is_void = ret_is_void,
            c_decl_str = ret_clang_type.spelling,
            clang_type = ret_clang_type,
            )

        args: list[Arg] = []
        for child in children:
            args.append(Arg(
                name = child.spelling,
                c_decl_str = stringify_argument_type_and_name(child.type, child.spelling),
                clang_type = child.type,
                clang_decl = child,
                ))

        methods.append(Method(
            name = c.spelling,
            ret = ret,
            args = args,
            ))

    return StructInterface(
        name = struct.spelling,
        methods = methods,
        clang_decl = struct
        )
