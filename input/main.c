// Copyright 2022, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0

#include "xrt/xrt_compositor.h"
#include "util/u_pacing.h"


struct test
{
	int (*foo)(struct test *foo, int *const arg[][2]);
};

int main()
{
	return 0;
}
