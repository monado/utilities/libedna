#!/bin/env python3
# Copyright 2022, Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0

import sys
import os

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), 'src'))

from edna.iface import Method, StructInterface, Arg, Ret
from edna.clang import get_struct_interface_from_struct, filter_node_list_by_node_kind

from clang.cindex import Index, TranslationUnit, Cursor, CursorKind, Type, TypeKind
from dataclasses import dataclass
import typing

index: Index
"""
Global shared index object
"""


def load_input_file() -> TranslationUnit:
    absolute_script_path = os.path.abspath(__file__)
    absolute_dir = os.path.dirname(absolute_script_path)
    input_file = absolute_dir + '/input/main.c'
    monado_dir = os.path.abspath(absolute_dir + '/../Monado')

    print(f'Loading file "{input_file}"')

    index = Index.create()
    translation_unit = index.parse(
        input_file,
        args=[
            '-xc', f'-I{monado_dir}/src/xrt/include',
            f'-I{monado_dir}/src/xrt/auxiliary'
        ])

    for diag in translation_unit.diagnostics:
        print(diag)

    return translation_unit



translation_unit = load_input_file()

clang_structs = filter_node_list_by_node_kind(
    translation_unit.cursor.get_children(),
    [CursorKind.CLASS_DECL, CursorKind.STRUCT_DECL])

classes: [] = list()

for clang_struct in clang_structs:
    classes.append(get_struct_interface_from_struct(clang_struct))



def snake_to_c_camle_case(s: str):
    components = s.split('_')
    return 'c' + ''.join(x.title() for x in components)


def write_cpp_methods(si: StructInterface):
    file = sys.stdout

    file.write(f'class {si.name}\n')
    for m in si.methods:
        file.write(f'\tXRT_C_CALLING static {m.ret.c_decl_str}\n')
        file.write(f'\t{snake_to_c_camle_case(m.name)}(')

        arg_strs = []
        for arg in m.args:
            arg_strs.append(arg.c_decl_str)

        file.write(', '.join(arg_strs))
        file.write(');\n')


for c in classes:
    write_cpp_methods(c)


